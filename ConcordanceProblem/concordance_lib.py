'''
Created on Mar 4, 2015

@author: Alex Volkov
@contact: volkoa@gmail.com
'''
# ------------------------------------------------------ Python Standad Library
import string
import re
# --------------------------- Python Non-Standad Modules and Packages from PyPi
from sortedcontainers import SortedDict


__all__ = ('concordance_obj',)


class WrdMapSchema(object):
    '''Defines entries in the concordance word map. Ex.:
        {
            'word': {
                'wrd_freq': 0,
                'in_sent': []
            },
            ... more word entries
        }
    '''
    wrd_freq = 'wrd_freq'  # cound word occurence frequency
    in_sent = 'in_sent'  # list of sentences where the word occurs

    @classmethod
    def empty_wrd_ent(cls):
        return {'wrd_freq': 0, 'in_sent': []}

    @classmethod
    def empty_wrd_map(cls):
        '''Structure of wrd_map would enventually look per docstring of
        WrdMapSchema. Initially it's an instance of SortedDict container.
        '''
        return SortedDict()

    @classmethod
    def add_wrd_ent(cls, wrd_map, wrd, cur_sent):
        '''
        :param wrd_map: SortedDict. Mutable and extended in-place.
        '''
        wrd_ent = \
            wrd_map.setdefault(wrd, cls.empty_wrd_ent())
        wrd_ent[WrdMapSchema.wrd_freq] += 1
        wrd_ent[WrdMapSchema.in_sent] += [cur_sent]


class concordance_obj(object):
    # Using classmethods because this object is stateless. This class is used
    # for namespacing primarily.

    @classmethod
    def concordance_proc(cls, fileobj):
        '''Given an arbitrary text document written in English, generates a
        concordance, i.e. an alphabetical list of all word occurrences, labeled
        with word frequencies. Each word is labeled with the sentence numbers
        in which each occurrence appeared.

        :param fileobj: file-like object. Read this file entity line-by-line.

        :returns: Word map. Refer to WrdMapSchema for the word map structure.
        :rtype: SortedDict
        '''
        abbreviations = ('dr.', 'mr.', 'bro.', 'mrs.', 'ms.', 'jr.', 'sr.',
                         'i.e.', 'e.g.', 'vs.')
        sentence_enders = ('.', '!', '?', '...')
        # chars_to_remove mostly in string.punctuation except for sentence
        # enders.
        chars_to_remove = (':', ';', '~', '@', '#', '$', '%', '^', '&', '*',
                           '(', ')', '>', '<', '{', '}', '[', ']', '|', '\\',
                           '/', '_', '\'', '"')
        dig_set = set(string.digits)

        wrd_map = WrdMapSchema.empty_wrd_map()
        cur_sent = 1  # Keep track of the sentence in which a word appears.
        # process line-by-line
        for ln in fileobj:
            # skip blank lines
            if not ln.strip():
                continue

            ln_lower = ln.lower()
            # :  :type ln_lower: str

            # remove characters that are not words
            for rm_c in chars_to_remove:
                # print re.escape('{}'.format(rm_c))
                ln_lower = re.sub(re.escape('{}'.format(rm_c)), ' ', ln_lower)

            # Split by splaces into individual words to process.
            wrd_list = ln_lower.split()

            # process each word now
            for wrd in wrd_list:
                # process abbreviations first
                if wrd in abbreviations:
                    WrdMapSchema.add_wrd_ent(wrd_map, wrd, cur_sent)
                    continue

                # Clean the word by removing anything that's not alphanumeric.
                wrd_cln = re.sub(r'[^\w]', '', wrd)

                # If the word contains a number ignore it.
                if not set(wrd_cln).intersection(dig_set):
                    WrdMapSchema.add_wrd_ent(wrd_map, wrd_cln, cur_sent)

                # check for sentence enders
                for se in sentence_enders:
                    if wrd.endswith(se):
                        cur_sent += 1
                        break

        return wrd_map

    @classmethod
    def concordance_write(cls, wrd_map, fileobj):
        '''Writes out the concordance to the file entity. The format output is:
            some_word {frequency: in sent_x, in sent_x, in sent_y, ...
        For text_input.txt used in testing a line would look as follows:
            word {3: 1, 1, 2}

        :param wrd_map: SortedDict. Word map. Refer to WrdMapSchema for the
            word map structure.
        :param fileobj: file-like object. Write to this file entity.
        '''
        for wrd, wrd_ent in wrd_map.items():
            print >> fileobj, '{wrd} {{{wrd_freq}: {in_sent}}}'.\
                format(
                    wrd=wrd, wrd_freq=wrd_ent['wrd_freq'],
                    in_sent=''.
                    join(re.split('\[(.*?)\]', str(wrd_ent['in_sent'])))
                )
