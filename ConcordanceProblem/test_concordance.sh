#!/bin/bash

# ref: http://stackoverflow.com/questions/5195607/checking-bash-exit-status-of-several-commands-efficiently

function test {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "error with $1" >&2
    else
        echo "pass"
    fi
    return $status
}

./concordance_cli.py text_input.txt > text_out.txt
test diff -w concordance_test.txt text_out.txt

