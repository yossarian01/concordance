#!/usr/bin/env python
# encoding: utf-8
'''
concordance_cli -- Generate a concordance for an arbitrary text document.

Desc.:
Given an arbitrary text document written in English, write a program that will
generate a concordance, i.e. an alphabetical list of all word occurrences,
labeled with word frequencies. Bonus: label each word with the sentence numbers
in which each occurrence appeared.

@author: Alex Volkov
@contact: volkoa@gmail.com
'''

# ------------------------------------------------------ Python Standad Library
import sys
# import os
from argparse import (
    ArgumentDefaultsHelpFormatter, HelpFormatter, RawDescriptionHelpFormatter,
    ArgumentParser)
from textwrap import dedent

import contextlib

# -------------------------------------------------------------- Custom modules
from concordance_lib import concordance_obj
# ------------------------------------------------ module functions and Classes


class SmartFormatterMixin(HelpFormatter):
# ref: http://stackoverflow.com/questions/3853722/python-argparse-how-to-insert-newline-in-the-help-text  @IgnorePep8
    def _split_lines(self, text, width):
        # this is the RawTextHelpFormatter._split_lines
        if text.startswith('S|'):
            return text[2:].splitlines()

        # return HelpFormatter._split_lines(self, text, width)
        return super(SmartFormatterMixin, self)._split_lines(text, width)


class CustomFormatter(ArgumentDefaultsHelpFormatter,
                      RawDescriptionHelpFormatter,
                      SmartFormatterMixin):
    ''' Convenience formatter_class for argparse help print out.'''


# ref: http://stackoverflow.com/questions/17602878/how-to-handle-both-with-open-and-sys-stdout-nicely @IgnorePep8
@contextlib.contextmanager
def smart_open(filename=None):
    if filename and filename != '-':
        fh = open(filename, 'w')
    else:
        fh = sys.stdout

    try:
        yield fh
    finally:
        if fh is not sys.stdout:
            fh.close()


def main(argv=None):
    '''
    Given an arbitrary text document written in English, write a program
    that will generate a concordance, i.e. an alphabetical list of all word
    occurrences, labeled with word frequencies. Bonus: label each word with the
    sentence numbers in which each occurrence appeared. Invoke ex.:
    $ python concordance_cli.py text_input.txt

    For text_input.txt file provided with this cli the output would look
    something like:
        a {2: 1, 1}
        all {1: 1}
        alphabetical {1: 1}
        ...
    '''
    argv = sys.argv if argv is None else sys.argv.extend(argv)
    desc = main.__doc__

    # CLI parser
    parser = ArgumentParser(description=dedent(desc),
                            formatter_class=CustomFormatter)
    parser.add_argument(
        'text_file_in', type=str,
        help='S|A file written in English to generate a concordance for.')
    parser.add_argument(
        'text_file_out', type=str, nargs='?',
        help='S|File where to write out the concordance. If not specified\n'
             'then streamed to stdout.')

    args = parser.parse_args()
    text_file_in = args.text_file_in
    text_file_out = args.text_file_out

    # ---------------------------------------------------- Calcuate Concordance
    concordance_inst = concordance_obj()
    # Process the text file.
    with open(text_file_in) as tfi:
        wrd_map = concordance_inst.concordance_proc(tfi)

    # Write out to stdout or text file.
    with smart_open(text_file_out) as tfo:
        concordance_inst.concordance_write(wrd_map, tfo)

if __name__ == '__main__':
    main()
